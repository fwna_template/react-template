import React, { Component } from 'react';
import RouterView from './Router/index.jsx';
import {BrowserRouter as Router} from 'react-router-dom';
import  css from "./App.css"
class Index extends Component {

    render() {
        return (
            <div className={css.container}> 
             <Router>
            <RouterView></RouterView>
           </Router>
           </div>
            
           
        );
    }
}

export default Index;
