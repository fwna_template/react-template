import { useState } from 'react';
import MovieHttp from "../request/MovieHttp.js";
const SetHooks=()=>{
    const [intheaters,setIntheaters]=useState([]);
    const getMovie=async ()=>{
        let intheaters=await MovieHttp.inTheaters();
        setIntheaters(()=>{
           intheaters=intheaters.data.res.slice(0,7)
           return [...intheaters]
        })
    }
    return [intheaters,getMovie]
}


export default SetHooks