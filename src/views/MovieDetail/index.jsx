import React,{useEffect,useState} from 'react';
import { useSearchParams,useNavigate } from 'react-router-dom';
import MovieHttp from "../../request/MovieHttp.js";

const Index = () => {
    const navigate = useNavigate();
    const [data,setData]=useState({data:{}})
    let [query] = useSearchParams();
    const getdetail=async (id)=>{
      let res=await MovieHttp.detail(id);
     let data=(res.data.res[0]);
      setData({...data,data});
      console.log(data);
    }
    useEffect(()=>{
        console.log(query.get("id"));
        getdetail(query.get("id"));
    },[])
    return (
        <div>
             <div>{data.title}</div>
            <div onClick={()=>{navigate(-1)}}>返回</div>
        </div>
    );
}

export default Index;
