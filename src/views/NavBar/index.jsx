import React from 'react';
import {NavLink} from 'react-router-dom'
import "./index.css"
import css from './index.module.css';
const Index = () => {
    return (
        <div className={css.nav}>
               <NavLink to='/'>电影</NavLink>
               <NavLink to='/user'>用户</NavLink>
        </div>
    );
}

export default Index;
