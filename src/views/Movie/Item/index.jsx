import React,{useContext} from 'react';
import {useNavigate} from 'react-router-dom'
import css from './index.module.css';
import {useMsgContect} from "../../../Content"
const Index = (props) => {
    const value=useContext(useMsgContect);
    const navigate = useNavigate();
    const Todetail=(value)=>{
        console.log(value);
        navigate(`/detail?id=${value}`)
    }
    const List=props.data.map(item=>{
        return (
            <div key={item._id} className={css.item}>

            <div onClick={Todetail.bind(this,item._id)} className={css.image}><img src={item.pic} alt="" /></div>
            <div onClick={Todetail.bind(this,item._id)} className={css.contain}>
                 <div>{item.title}</div>
                 <div>观众评分<span>{item.raiting}</span></div>
                 <div>{item.slogo}</div>
                 <div><span>{item.labels[2]}</span></div>
            </div>  
            <div className={css.buy}>
                <button>购票</button>
            </div>
        </div>
        )
    })
    return (
        <div>
            {
                List
            }
        </div>
    );
}

export default Index;
