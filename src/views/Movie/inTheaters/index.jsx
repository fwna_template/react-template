import React, { useEffect } from 'react';
import Item from '../Item';
import SetHooks from '../../../Hooks/CountHook.jsx';
import {useMsgContect} from "../../../Content"
const Index = () => {
   const [intheaters,getMovie]=SetHooks();
   useEffect(()=>{ getMovie()},[])
    return (
        <div>
            <useMsgContect.Provider value={'msg'}>
            <Item   data={intheaters}/>
            </useMsgContect.Provider>
           
        </div>
    );
}

export default Index;
