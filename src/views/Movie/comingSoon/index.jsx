import React, { useEffect,useState } from 'react';
import MovieHttp from "../../../request/MovieHttp.js";
import Item from '../Item';
const Index = () => {
    const [comingSoon,setComingSoon]=useState([]);
    const getMovie=async ()=>{
        let comingSoon=await MovieHttp.comingSoon();
        setComingSoon(()=>{
            comingSoon=comingSoon.data.res;
            console.log(comingSoon);
            return [...comingSoon]
         })
   }
   useEffect(()=>{ 
      console.log(this);
       getMovie();
      
     },[])
    return (
        <div>
            <Item data={comingSoon} />
        </div>
    );
}

export default Index;