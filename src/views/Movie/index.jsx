import React from 'react';
import NavBar from "../NavBar"
import {Outlet,NavLink} from 'react-router-dom'
import css from './index.module.css';
import Bnner from "./Banner"
import useMyReducer from '../../Reducer';
import {useNavigate} from 'react-router-dom'
const Index = () => {
    const navigate = useNavigate();
    const {state}=useMyReducer();
    const toCities=()=>{
        navigate("/city");
    }
    return (
        <div>
            <div onClick={toCities}>{state.city}</div>
           <div>
               {/*  */}
                <Bnner></Bnner>
               {/*  */}
           </div>

           <div className={css.title}> <NavLink to="/InTheaters">InTheaters</NavLink>
                 <NavLink to="/ComingSoon">ComingSoon</NavLink></div>
           <div></div>
           <Outlet></Outlet>
           <NavBar></NavBar>
        </div>
    );
   

}

export default Index;
