import React from 'react';
import { Carousel } from 'antd';
import css from './index.module.css';
const Index = () => {

  const contentStyle = {
    height: '160px',
    with:"100%",
    color: '#fff',
    lineHeight: '160px',
    background: '#364d79',
  };
  return (
    <div>
        <Carousel autoplay>
    <div style={contentStyle}>
      <img className={css.image} src="https://static.maizuo.com/v5/upload/7572cf53619677bedf1a657b0d295afc.jpg?x-oss-process=image/quality,Q_70" alt="" />
    </div>
    <div style={contentStyle}>
      <img className={css.image}  src="https://static.maizuo.com/v5/upload/b7c056fa2ec0df3702b526f7535ab493.jpg?x-oss-process=image/quality,Q_70" alt="" />
    </div>
    <div style={contentStyle}>
     <img className={css.image} src="https://static.maizuo.com/v5/upload/f6cc1510a17ce3d424fac9a88c87db03.jpg?x-oss-process=image/quality,Q_70" alt="" />
    </div>
  </Carousel>
    </div>
  );
}

export default Index;







