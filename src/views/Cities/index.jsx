import React,{useState} from 'react';
import useMyReducer from '../../Reducer';
import {useNavigate} from 'react-router-dom'
const Index = () => {
    const navigate = useNavigate();
    const [city,setCity]=useState(["广州","无锡","西安"])
    const {dispatch}=useMyReducer();
    const handleCity=(value)=>{
        console.log(value);
        dispatch({type:"changecity",value});
        navigate(-1)
    }
    return (
        <div>
             {
                 city.map(item=>{
                     return(  <button onClick={handleCity.bind(this,item)}>{item}</button>)
                 })
             }
        </div>
    );
}

export default Index;
