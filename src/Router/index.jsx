import {useRoutes} from 'react-router-dom'

import Movie from "../views/Movie"
import User from "../views/User"
import InTheaters from '../views/Movie/inTheaters';
import ComingSoon from '../views/Movie/comingSoon';
import NotFound from '../views/NotFound';
import MovieDetail from '../views/MovieDetail';
import Cities from '../views/Cities';
const RouterView=()=>{
     const routes=useRoutes([
        {
            path: "/",
            element: <Movie />,
            children:[
                {
                    index: true,
                    element:<InTheaters/>
                },
                {   
                    index: true,
                    path: "InTheaters",
                    element:<InTheaters/>
                }, {
                    index: false,
                    path: "comingSoon",
                    element:<ComingSoon/>
                },
            ]
        },
        {
            path: "/user",
            element: <User />
        },
        {
            path: "/detail",
            element: <MovieDetail />
        },
        {
            path: "/city",
            element: <Cities />
        },
        {
            path: "*",
            element:<NotFound/>
        }
  
       
     ])
     return routes;
}

export default RouterView