import {useReducer} from "react";
const getDefaultCity=()=>{
       return localStorage.getItem("city")||"武汉"

}

const useMyReducer=()=>{
      const initState={
         city:getDefaultCity()
     }
     const reducer=(state,action)=>{
         switch(action.type){
             case "changecity":
                 console.log(action);
                let {value}=(action);
                localStorage.setItem("city",value)
                 return {
                      city:value
                 }
            default:
                return initState
         }
     }
     const [state,dispatch]=useReducer(reducer,initState);
     return {state,dispatch}
}

export default useMyReducer