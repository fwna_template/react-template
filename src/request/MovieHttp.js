import http from './http.js';
class MovieHttp extends http{
    static async  inTheaters(){
         return this.getHttp("4000/api/movie/inTheaters")
    }
    static async  comingSoon(){
        return this.getHttp("4000/api/movie/comingSoon")
    }
    static async  detail(id){
        return this.getHttp(`4000/api/movieDetail?id=${id}`)
    }
    static async  getImg(){
        return this.getHttp(`4000/api/banner`)
    }
}
export default MovieHttp;